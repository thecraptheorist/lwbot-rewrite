module.exports.run = async (client, message, args) => {
  var folder = args[0];
  var cmd = args[1];

  if(!folder) return message.channel.send(':x: **Missing a folder!**');
  if(!cmd) return message.channel.send(':x: **Missing command name!**');

  const response = await client.loadCommand(folder, cmd);
  if(response === false) return message.channel.send(`:white_check_mark: **Loaded ${args[0]}/${args[1]}**`);
  else message.channel.send(`:x: **${response}**`);
};

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: ['registercommand', 'addcommand', 'addcmd', 'register'],
  permLevel: 'Bot Owner'
};

exports.help = {
  name: 'registercmd',
  description: 'Register a command in the available commands',
  usage: 'registercmd <folder (no /)> <command name (no .js)>',
  category: 'System'
};